;; Controller admin definition of colt
;; Please add your license header here.
;; This file is generated automatically by GNU Artanis.
(define-artanis-controller admin) ; DO NOT REMOVE THIS LINE!!!

(use-modules (colt config))

(define (->admin what)
  (format #f "~a/~a" (colt-conf-get 'admin-url) what))

(define *login-path* (->admin "login"))
(define *auth-path* (->admin "auth"))
(define *admin-path* (->admin ""))

(get *login-path*
  (lambda (rc)
    (let ((failed (params rc "login_failed")))
      (view-render "login.html.tpl" (the-environment)))))

(post *auth-path*
  #:auth '(table user "user" "passwd")
  #:session #t
  (lambda (rc)
    #t))

(get *admin-path*
  #:session #t
  (lambda (rc)
    (cond
     ((:session rc 'check (view-render "admin.html.tpl" (the-environment)))
      (else (redirect-to rc *login-path*))))))
