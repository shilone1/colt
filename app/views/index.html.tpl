<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <@css bootstrap.min.css %>
    <@css blog.css %>
    <@js  jquery-3.3.1.min.js %>
    <@js  bootstrap.min.js %>

      <title> <%= blog-name %> </title>
  </head>

  <body>

    
    <div class="blog-masthead">
      <div class="container">
        <nav class="nav blog-nav">
          <a class="nav-link active" href="#">Home</a>
          <a class="nav-link" href="#">New features</a>
          <a class="nav-link" href="#">Press</a>
          <a class="nav-link" href="#">New hires</a>
          <a class="nav-link" href="#">About</a>
        </nav>
      </div>
    </div>
    
    <%= post-content %>
    
  </body>
</html>
